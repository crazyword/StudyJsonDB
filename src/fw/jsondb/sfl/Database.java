package fw.jsondb.sfl;

import java.io.*;
import java.util.*;

import com.alibaba.fastjson.*;
import com.alibaba.fastjson.serializer.SerializerFeature;

public class Database extends JSONObject{
	private String path;
	private String name;
	private JSONObject db = null;
	private File dbFile = null;
	private FileWriter fw = null; 
	
	/**
	 * 创建数据库对象，如果数据文件已经存在，则装入现有数据，否则创建该文件。
	 * @param path 数据库所在文件路径（以后可扩展为数据库实例的路径）。
	 * @param name 数据库名，对应数据文件为数据库名.json
	 * @throws IOException
	 */
	public Database(String path, String name) throws IOException {
		this.path = path;
		this.name = name;
		dbFile = new File(path + "/" + name + ".json");
		if(dbFile.exists()){
			loadData();
		}else{
			initData();
		}
	}
	
	private void loadData() throws IOException{
		InputStream in = new FileInputStream(dbFile);
		byte[] bytes = new byte[(int)dbFile.length()];
		in.read(bytes);
		String json = new String(bytes, "utf-8");
		db = JSON.parseObject(json, JSONObject.class);
	}
	
	private void saveData() throws IOException{

		fw = new FileWriter(dbFile, false);
		JSON.writeJSONStringTo(db, fw, SerializerFeature.PrettyFormat);
		fw.flush();
	}
	
	/**
	 * 关闭数据库
	 * @throws IOException
	 */
	public void close() throws IOException{
		saveData();
		fw.close();
	}
	
	private void initData() throws IOException{
		db = new JSONObject();
		db.put("type", "database");
		db.put("name", this.name);
		db.put("tables", new JSONObject());
		saveData();
	}
	
	public JSONObject getRoot(){
		return db;
	}
	
	/**
	 * 创建表
	 * @param tbName 表名
	 * @throws IOException 保存数据时可能出现异常
	 */
	public void createTable(String tbName) throws IOException{
		JSONObject tb = new JSONObject();
		tb.put("type", "table");
		tb.put("name", tbName);
		tb.put("data", new JSONObject());
		
		JSONObject tables = (JSONObject)db.get("tables");
		tables.put(tbName, tb);
		
		saveData();
	}
	
	/**
	 * 删除表
	 * @param tbName 表名
	 * @throws IOException 保存数据时可能出现异常
	 */
	public void dropTable(String tbName) throws IOException{
		JSONObject tables = (JSONObject)db.get("tables");
		tables.remove(tbName);
		
		saveData();
	}
	
	private JSONObject getTable(String tbName){
		JSONObject tables = (JSONObject)db.get("tables");
		return (JSONObject)tables.get(tbName);
	}
	
	private JSONObject getTableRecords(String tbName){
		JSONObject tb = this.getTable(tbName);
		return (JSONObject)tb.get("data");
	}
	
	/**
	 * 插入新记录
	 * @param tbName 表名
	 * @param jso 新值。新值中如果没有_id字段，则自动插入一个uuid作为_id的值。
	 * @return 插入的记录数
	 * @throws IOException 保存数据时可能出现异常
	 */
	public int insert(String tbName, JSONObject jso) throws IOException{
		JSONObject tbData = (JSONObject)this.getTableRecords(tbName);
		if(!jso.containsKey("_id")){
			jso.put("_id", UUID.randomUUID().toString());
		}
		tbData.put(jso.get("_id").toString(), jso);
		
		saveData();
		return 1;
	}
	
	/**
	 * 删除表中符合条件的记录
	 * @param tbName 表名
	 * @param condition 条件JSO对象
	 * @return 被删除的记录条数
	 * @throws IOException 保存数据时可能发生的异常
	 */
	public int delete(String tbName, JSONObject condition) throws IOException{
		int result = 0;
		JSONObject tbData = (JSONObject)this.getTableRecords(tbName);
		
		List<JSONObject> jsos = this.find(tbName, condition);
		for(JSONObject jso : jsos){
			tbData.remove(jso.get("_id"));
			result++;
		}
		
		saveData();
		
		return result;
	}
	
	/**
	 * 更新表中的记录，当表中的记录满足第二个参数condition所表示的条件时，用第三个参数newJso作为新值替代原有值。
	 * @param tbName 表名
	 * @param condition 条件JSO对象
	 * @param newJso 新值JSO对象
	 * @return 更新的记录数量
	 * @throws IOException 当保存数据时可能发生异常
	 */
	public int update(String tbName, JSONObject condition, JSONObject newJso) throws IOException{
		int result = 0;
		JSONObject tbData = (JSONObject)this.getTableRecords(tbName);
		
		List<JSONObject> jsos = this.find(tbName, condition);
		for(JSONObject jso : jsos){
			//用原来的_id覆盖新值的_id，以保持原来的_id不发生变化。
			newJso.put("_id", jso.get("_id").toString()); 
			
			tbData.put(jso.get("_id").toString(), newJso);
			result++;
		}
		
		saveData();
		
		return result;
	}
	
	public List<Object> find(String tbName){
		List<Object> result = new ArrayList<>();
		JSONObject tbData = (JSONObject)this.getTableRecords(tbName);
		
		for(Object v : tbData.values()){
			result.add(v);
		}
		
		return result;
	}
	
	/**
	 * 遍历并查找与condition中所有属性匹配的对象。
	 * @param tbName 表名
	 * @param condition 查询条件对象
	 * @return JSO对象列表
	 */
	public List<JSONObject> find(String tbName, JSONObject condition){
		List<JSONObject> result = new ArrayList<>();
		JSONObject tbData = (JSONObject)this.getTableRecords(tbName);
		
		for(String uuid : tbData.keySet()){
			JSONObject tofind = (JSONObject)tbData.get(uuid);
			if(this.match(tofind, condition)){
				result.add(tofind);
			}
		}
		
		return result;
	}
	
	/**
	 * 判断第一个参数中的属性是否符合第二个参数的条件。
	 * @param jso 被判断JSO对象
	 * @param condition 条件JSO对象
	 * @return 当第二个参数中所有属性都可以在第一个参数中找到，并且属性值与之相等时，返回true，否则false。
	 */
	private boolean match(JSONObject jso, JSONObject condition){
		boolean result = true;
		for(Object key: condition.keySet()){
			if(jso.containsKey(key) && jso.get(key).equals(condition.get(key))){
				continue;
			}else{
				result = false;
				break;
			}
		}
		return result;
	}
}
