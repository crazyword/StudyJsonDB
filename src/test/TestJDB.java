package test;

import java.io.IOException;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import fw.jsondb.sfl.*;

public class TestJDB {

	public static void main(String[] args) {
		if(args.length==0){
			System.out.println("need args: instance path(a folder).");
			return;
		}
		
		try {
			
			Database db = new Database(args[0], "testDB");
			
			//创建表
			System.out.println("创建表...");
			String tbName = "person";
			db.dropTable(tbName);
			db.createTable(tbName);
			System.out.println(JSON.toJSONString(db.getRoot()));
			
			//插入数据
			System.out.println("插入数据...");
			JSONObject jack = new JSONObject();
			jack.put("name", "Jack");
			jack.put("phone", "12345678");
			db.insert(tbName, jack);
			
			JSONObject mary = new JSONObject();
			mary.put("name", "Mary");
			mary.put("address", "北京东城");
			db.insert(tbName, mary);
			
			//查询所有数据
			System.out.println("查询所有数据...");
			db.find(tbName).forEach(System.out::println);
			
			//更新数据
			System.out.println("更新数据...");
			JSONObject newMary = new JSONObject();
			newMary.put("name", "Mary");
			newMary.put("address", "北京西城");
			db.update(tbName, mary, newMary);
			
			//指定条件查询
			System.out.println("指定条件查询...");
			JSONObject condition1 = new JSONObject();
			condition1.put("name", "Mary");
			db.find(tbName, condition1).forEach(System.out::println);
			
			//删除指定数据
			System.out.println("删除指定数据...");
			JSONObject condition2 = new JSONObject();
			condition2.put("name", "Jack");
			db.delete(tbName, condition2);
			db.find(tbName).forEach(System.out::println);
			
			//关闭数据库
			System.out.println("关闭数据库...");
			db.close();
			
			System.out.println("测试结束。");
			
		} catch (IOException e) {

			e.printStackTrace();
		}
	}
}
